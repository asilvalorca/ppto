<?php

echo "<style>
#fountainTextG{
width:700px;
}

.fountainTextG{
color:#000000;
font-family:Arial;
font-size:30px;
text-decoration:none;
font-weight:normal;
font-style:normal;
float:left;
-moz-animation-name:bounce_fountainTextG;
-moz-animation-duration:2.6s;
-moz-animation-iteration-count:infinite;
-moz-animation-direction:linear;
-moz-transform:scale(.5);
-webkit-animation-name:bounce_fountainTextG;
-webkit-animation-duration:2.6s;
-webkit-animation-iteration-count:infinite;
-webkit-animation-direction:linear;
-webkit-transform:scale(.5);
-ms-animation-name:bounce_fountainTextG;
-ms-animation-duration:2.6s;
-ms-animation-iteration-count:infinite;
-ms-animation-direction:linear;
-ms-transform:scale(.5);
-o-animation-name:bounce_fountainTextG;
-o-animation-duration:2.6s;
-o-animation-iteration-count:infinite;
-o-animation-direction:linear;
-o-transform:scale(.5);
animation-name:bounce_fountainTextG;
animation-duration:2.6s;
animation-iteration-count:infinite;
animation-direction:linear;
transform:scale(.5);
}

#fountainTextG_1{
-moz-animation-delay:0.2s;
-webkit-animation-delay:0.2s;
-ms-animation-delay:0.2s;
-o-animation-delay:0.2s;
animation-delay:0.2s;
}

#fountainTextG_2{
-moz-animation-delay:0.25s;
-webkit-animation-delay:0.25s;
-ms-animation-delay:0.25s;
-o-animation-delay:0.25s;
animation-delay:0.25s;
}

#fountainTextG_3{
-moz-animation-delay:0.3s;
-webkit-animation-delay:0.3s;
-ms-animation-delay:0.3s;
-o-animation-delay:0.3s;
animation-delay:0.3s;
}

#fountainTextG_4{
-moz-animation-delay:0.35s;
-webkit-animation-delay:0.35s;
-ms-animation-delay:0.35s;
-o-animation-delay:0.35s;
animation-delay:0.35s;
}

#fountainTextG_5{
-moz-animation-delay:0.4s;
-webkit-animation-delay:0.4s;
-ms-animation-delay:0.4s;
-o-animation-delay:0.4s;
animation-delay:0.4s;
}

#fountainTextG_6{
-moz-animation-delay:0.45s;
-webkit-animation-delay:0.45s;
-ms-animation-delay:0.45s;
-o-animation-delay:0.45s;
animation-delay:0.45s;
}

#fountainTextG_7{
-moz-animation-delay:0.5s;
-webkit-animation-delay:0.5s;
-ms-animation-delay:0.5s;
-o-animation-delay:0.5s;
animation-delay:0.5s;
}

#fountainTextG_8{
-moz-animation-delay:0.55s;
-webkit-animation-delay:0.55s;
-ms-animation-delay:0.55s;
-o-animation-delay:0.55s;
animation-delay:0.55s;
}

#fountainTextG_9{
-moz-animation-delay:0.6s;
-webkit-animation-delay:0.6s;
-ms-animation-delay:0.6s;
-o-animation-delay:0.6s;
animation-delay:0.6s;
}

#fountainTextG_10{
-moz-animation-delay:0.65s;
-webkit-animation-delay:0.65s;
-ms-animation-delay:0.65s;
-o-animation-delay:0.65s;
animation-delay:0.65s;
}

#fountainTextG_11{
-moz-animation-delay:0.7s;
-webkit-animation-delay:0.7s;
-ms-animation-delay:0.7s;
-o-animation-delay:0.7s;
animation-delay:0.7s;
}

#fountainTextG_12{
-moz-animation-delay:0.75s;
-webkit-animation-delay:0.75s;
-ms-animation-delay:0.75s;
-o-animation-delay:0.75s;
animation-delay:0.75s;
}

#fountainTextG_13{
-moz-animation-delay:0.8s;
-webkit-animation-delay:0.8s;
-ms-animation-delay:0.8s;
-o-animation-delay:0.8s;
animation-delay:0.8s;
}

#fountainTextG_14{
-moz-animation-delay:0.85s;
-webkit-animation-delay:0.85s;
-ms-animation-delay:0.85s;
-o-animation-delay:0.85s;
animation-delay:0.85s;
}

#fountainTextG_15{
-moz-animation-delay:0.9s;
-webkit-animation-delay:0.9s;
-ms-animation-delay:0.9s;
-o-animation-delay:0.9s;
animation-delay:0.9s;
}

#fountainTextG_16{
-moz-animation-delay:0.95s;
-webkit-animation-delay:0.95s;
-ms-animation-delay:0.95s;
-o-animation-delay:0.95s;
animation-delay:0.95s;
}

#fountainTextG_17{
-moz-animation-delay:1s;
-webkit-animation-delay:1s;
-ms-animation-delay:1s;
-o-animation-delay:1s;
animation-delay:1s;
}

#fountainTextG_18{
-moz-animation-delay:1.05s;
-webkit-animation-delay:1.05s;
-ms-animation-delay:1.05s;
-o-animation-delay:1.05s;
animation-delay:1.05s;
}

#fountainTextG_19{
-moz-animation-delay:1.1s;
-webkit-animation-delay:1.1s;
-ms-animation-delay:1.1s;
-o-animation-delay:1.1s;
animation-delay:1.1s;
}

#fountainTextG_20{
-moz-animation-delay:1.15s;
-webkit-animation-delay:1.15s;
-ms-animation-delay:1.15s;
-o-animation-delay:1.15s;
animation-delay:1.15s;
}

#fountainTextG_21{
-moz-animation-delay:1.2s;
-webkit-animation-delay:1.2s;
-ms-animation-delay:1.2s;
-o-animation-delay:1.2s;
animation-delay:1.2s;
}

#fountainTextG_22{
-moz-animation-delay:1.25s;
-webkit-animation-delay:1.25s;
-ms-animation-delay:1.25s;
-o-animation-delay:1.25s;
animation-delay:1.25s;
}

#fountainTextG_23{
-moz-animation-delay:1.3s;
-webkit-animation-delay:1.3s;
-ms-animation-delay:1.3s;
-o-animation-delay:1.3s;
animation-delay:1.3s;
}

#fountainTextG_24{
-moz-animation-delay:1.35s;
-webkit-animation-delay:1.35s;
-ms-animation-delay:1.35s;
-o-animation-delay:1.35s;
animation-delay:1.35s;
}

#fountainTextG_25{
-moz-animation-delay:1.4s;
-webkit-animation-delay:1.4s;
-ms-animation-delay:1.4s;
-o-animation-delay:1.4s;
animation-delay:1.4s;
}

#fountainTextG_26{
-moz-animation-delay:1.45s;
-webkit-animation-delay:1.45s;
-ms-animation-delay:1.45s;
-o-animation-delay:1.45s;
animation-delay:1.45s;
}

#fountainTextG_27{
-moz-animation-delay:1.5s;
-webkit-animation-delay:1.5s;
-ms-animation-delay:1.5s;
-o-animation-delay:1.5s;
animation-delay:1.5s;
}

#fountainTextG_28{
-moz-animation-delay:1.55s;
-webkit-animation-delay:1.55s;
-ms-animation-delay:1.55s;
-o-animation-delay:1.55s;
animation-delay:1.55s;
}

#fountainTextG_29{
-moz-animation-delay:1.6s;
-webkit-animation-delay:1.6s;
-ms-animation-delay:1.6s;
-o-animation-delay:1.6s;
animation-delay:1.6s;
}

#fountainTextG_30{
-moz-animation-delay:1.65s;
-webkit-animation-delay:1.65s;
-ms-animation-delay:1.65s;
-o-animation-delay:1.65s;
animation-delay:1.65s;
}

#fountainTextG_31{
-moz-animation-delay:1.7s;
-webkit-animation-delay:1.7s;
-ms-animation-delay:1.7s;
-o-animation-delay:1.7s;
animation-delay:1.7s;
}

#fountainTextG_32{
-moz-animation-delay:1.75s;
-webkit-animation-delay:1.75s;
-ms-animation-delay:1.75s;
-o-animation-delay:1.75s;
animation-delay:1.75s;
}

#fountainTextG_33{
-moz-animation-delay:1.8s;
-webkit-animation-delay:1.8s;
-ms-animation-delay:1.8s;
-o-animation-delay:1.8s;
animation-delay:1.8s;
}

#fountainTextG_34{
-moz-animation-delay:1.85s;
-webkit-animation-delay:1.85s;
-ms-animation-delay:1.85s;
-o-animation-delay:1.85s;
animation-delay:1.85s;
}

#fountainTextG_35{
-moz-animation-delay:1.9s;
-webkit-animation-delay:1.9s;
-ms-animation-delay:1.9s;
-o-animation-delay:1.9s;
animation-delay:1.9s;
}

#fountainTextG_36{
-moz-animation-delay:1.95s;
-webkit-animation-delay:1.95s;
-ms-animation-delay:1.95s;
-o-animation-delay:1.95s;
animation-delay:1.95s;
}

#fountainTextG_37{
-moz-animation-delay:2s;
-webkit-animation-delay:2s;
-ms-animation-delay:2s;
-o-animation-delay:2s;
animation-delay:2s;
}

#fountainTextG_38{
-moz-animation-delay:2.05s;
-webkit-animation-delay:2.05s;
-ms-animation-delay:2.05s;
-o-animation-delay:2.05s;
animation-delay:2.05s;
}

#fountainTextG_39{
-moz-animation-delay:2.1s;
-webkit-animation-delay:2.1s;
-ms-animation-delay:2.1s;
-o-animation-delay:2.1s;
animation-delay:2.1s;
}

#fountainTextG_40{
-moz-animation-delay:2.15s;
-webkit-animation-delay:2.15s;
-ms-animation-delay:2.15s;
-o-animation-delay:2.15s;
animation-delay:2.15s;
}

#fountainTextG_41{
-moz-animation-delay:2.2s;
-webkit-animation-delay:2.2s;
-ms-animation-delay:2.2s;
-o-animation-delay:2.2s;
animation-delay:2.2s;
}

#fountainTextG_42{
-moz-animation-delay:2.25s;
-webkit-animation-delay:2.25s;
-ms-animation-delay:2.25s;
-o-animation-delay:2.25s;
animation-delay:2.25s;
}

#fountainTextG_43{
-moz-animation-delay:2.3s;
-webkit-animation-delay:2.3s;
-ms-animation-delay:2.3s;
-o-animation-delay:2.3s;
animation-delay:2.3s;
}

#fountainTextG_44{
-moz-animation-delay:2.35s;
-webkit-animation-delay:2.35s;
-ms-animation-delay:2.35s;
-o-animation-delay:2.35s;
animation-delay:2.35s;
}

#fountainTextG_45{
-moz-animation-delay:2.4s;
-webkit-animation-delay:2.4s;
-ms-animation-delay:2.4s;
-o-animation-delay:2.4s;
animation-delay:2.4s;
}

#fountainTextG_46{
-moz-animation-delay:2.45s;
-webkit-animation-delay:2.45s;
-ms-animation-delay:2.45s;
-o-animation-delay:2.45s;
animation-delay:2.45s;
}

#fountainTextG_47{
-moz-animation-delay:2.5s;
-webkit-animation-delay:2.5s;
-ms-animation-delay:2.5s;
-o-animation-delay:2.5s;
animation-delay:2.5s;
}

#fountainTextG_48{
-moz-animation-delay:2.55s;
-webkit-animation-delay:2.55s;
-ms-animation-delay:2.55s;
-o-animation-delay:2.55s;
animation-delay:2.55s;
}

@-moz-keyframes bounce_fountainTextG{
0%{
-moz-transform:scale(1);
color:#000000;
}

100%{
-moz-transform:scale(.5);
color:#FFFFFF;
}

}

@-webkit-keyframes bounce_fountainTextG{
0%{
-webkit-transform:scale(1);
color:#000000;
}

100%{
-webkit-transform:scale(.5);
color:#FFFFFF;
}

}

@-ms-keyframes bounce_fountainTextG{
0%{
-ms-transform:scale(1);
color:#000000;
}

100%{
-ms-transform:scale(.5);
color:#FFFFFF;
}

}

@-o-keyframes bounce_fountainTextG{
0%{
-o-transform:scale(1);
color:#000000;
}

100%{
-o-transform:scale(.5);
color:#FFFFFF;
}

}

@keyframes bounce_fountainTextG{
0%{
transform:scale(1);
color:#000000;
}

100%{
transform:scale(.5);
color:#FFFFFF;
}

}
</style>";
echo '<div id="load" style="display:block; margin-left: 300;  width:80%; text-align:center; margin-top: 300px;">
<div id="fountainTextG">
<div id="fountainTextG_1" class="fountainTextG">
C</div>
<div id="fountainTextG_2" class="fountainTextG">
a</div>
<div id="fountainTextG_3" class="fountainTextG">
r</div>
<div id="fountainTextG_4" class="fountainTextG">
g</div>
<div id="fountainTextG_5" class="fountainTextG">
a</div>
<div id="fountainTextG_6" class="fountainTextG">
n</div>
<div id="fountainTextG_7" class="fountainTextG">
d</div>
<div id="fountainTextG_8" class="fountainTextG">
o</div>
<div id="fountainTextG_9" class="fountainTextG">
&nbsp;
</div>
<div id="fountainTextG_10" class="fountainTextG">
P</div>
<div id="fountainTextG_11" class="fountainTextG">
r</div>
<div id="fountainTextG_12" class="fountainTextG">
e</div>
<div id="fountainTextG_13" class="fountainTextG">
s</div>
<div id="fountainTextG_14" class="fountainTextG">
u</div>
<div id="fountainTextG_15" class="fountainTextG">
p</div>
<div id="fountainTextG_16" class="fountainTextG">
u</div>
<div id="fountainTextG_17" class="fountainTextG">
e</div>
<div id="fountainTextG_18" class="fountainTextG">
s</div>
<div id="fountainTextG_19" class="fountainTextG">
t</div>
<div id="fountainTextG_20" class="fountainTextG">
o</div>
<div id="fountainTextG_21" class="fountainTextG">
.</div>
<div id="fountainTextG_22" class="fountainTextG">
&nbsp;
</div>
<div id="fountainTextG_23" class="fountainTextG">
E</div>
<div id="fountainTextG_24" class="fountainTextG">
s</div>
<div id="fountainTextG_25" class="fountainTextG">
p</div>
<div id="fountainTextG_26" class="fountainTextG">
e</div>
<div id="fountainTextG_27" class="fountainTextG">
r</div>
<div id="fountainTextG_28" class="fountainTextG">
e</div>
<div id="fountainTextG_29" class="fountainTextG">
&nbsp;
</div>
<div id="fountainTextG_30" class="fountainTextG">
u</div>
<div id="fountainTextG_31" class="fountainTextG">
n</div>
<div id="fountainTextG_32" class="fountainTextG">
&nbsp;
</div>
<div id="fountainTextG_33" class="fountainTextG">
M</div>
<div id="fountainTextG_34" class="fountainTextG">
o</div>
<div id="fountainTextG_35" class="fountainTextG">
m</div>
<div id="fountainTextG_36" class="fountainTextG">
e</div>
<div id="fountainTextG_37" class="fountainTextG">
n</div>
<div id="fountainTextG_38" class="fountainTextG">
t</div>
<div id="fountainTextG_39" class="fountainTextG">
o</div>
<div id="fountainTextG_40" class="fountainTextG">
.</div>
<div id="fountainTextG_41" class="fountainTextG">
.</div>
<div id="fountainTextG_42" class="fountainTextG">
.</div>
<div id="fountainTextG_43" class="fountainTextG">
.</div>
<div id="fountainTextG_44" class="fountainTextG">
.</div>
<div id="fountainTextG_45" class="fountainTextG">
.</div>
<div id="fountainTextG_46" class="fountainTextG">
.</div>
<div id="fountainTextG_47" class="fountainTextG">
.</div>
<div id="fountainTextG_48" class="fountainTextG">
.</div>
</div> 

</div>';
?>