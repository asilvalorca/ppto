<?php
session_start(); //Iniciamos la session
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
include("conectar.php");
if(isset($_SESSION['usuario'] ))
{
		
	$nombre_usuario=$_SESSION['nombre']; // nombre del usuario
	$usuario=$_SESSION['usuario'];
	$id_usuario=$_SESSION['id_user'];
	$perfil=$_SESSION['tipouser']; //sacar perfil de usuario

?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<script language="javascript" src='js/jquery-1.8.3.min.js'></script><!--Incluye el framework de jquery -->
		<script language="javascript" src='js/jquery-ui-1.9.2.custom.min.js'></script><!--//Incluye el framework de jquery-ui-->
		<script language="javascript" src='js/funciones.js'></script><!-- contiene el validador de los input y la llamada el calendario (datepicker) -->
		<link rel='stylesheet' href='css/custom-theme2/jquery-ui-1.9.2.custom.min.css'><!-- Incluye el css del jquery UI-->
		<!--<link rel='stylesheet' href='css/ui-lightness/jquery-ui-1.9.2.custom.min.css'><!-- Incluye el css del jquery UI-->
		<link rel='stylesheet' href='css/styloPagina.css'><!-- Incluye el css del jquery UI
		<link rel="stylesheet" type="text/css" href="css/error.css"></link><!-- erro-->
		<link rel="stylesheet" type="text/css" href="css/nuevoboton.css"></link><!-- estilo del boton-->
		<!-- **********************************menu**********************************************************************************-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="menu/ddlevelsfiles/ddlevelsmenu-base.css" />
		<link rel="stylesheet" type="text/css" href="menu/ddlevelsfiles/ddlevelsmenu-topbar.css" />
		<link rel="stylesheet" type="text/css" href="menu/ddlevelsfiles/ddlevelsmenu-sidebar.css" />
		<script type="text/javascript" src="menu/ddlevelsfiles/ddlevelsmenu.js"></script>
		<!-- ********************************************************************************************************************-->
		<script type="text/javascript" src="js/alerta.js"></script>
<title>Procesar</title>
<style>
body
{

	background-color: #e7edf0;
	margin:0px;
	paddin: 0px;
}

#div_titulo
{

	width:60%;
	margin: 0 auto 0 auto;
	padding: 20px;
}

#principal
{
	border: 1px solid #CED5D7;
	border-radius: 6px;
	margin: 0 auto 0 auto;
	background-color: white;
	box-shadow: 0px 5px 10px #B5C1C5, 0 0 0 2px #EEF5F7 inset;
	width:60%;
	
}


.proceso label, .envio-correo label
{ 
		color: #6d6b6b;
		font-family: Calibri;
		font-weight:bold;
		display: block; 
		font-weight: bold;
}


.proceso div, .envio-correo div
{
	margin-bottom: 25px;
}

.titulo
{
	font-size: 20px; 
	text-align:center;
}
#main
{
	
	background-color: #002023;
	width:100%;
}

</style>

<script type="text/javascript">
function redireccionar() 
{
	var pagina="../index.php"
	location.href=pagina
} 

function redireccion()
{	
	$(document).empty();
	alert("No esta autorizado a ver el contenido");
	setTimeout ("redireccionar()", 60);
	}
</script>

<?php
//**********************funcion para asignar los nombres de los que estan con numeros*************
function cambio($mes)
{
			 if($mes=='1'){$mes_envio='Enero'; return $mes_envio;}
		else if($mes=='2'){$mes_envio='Febrero'; return $mes_envio;}
		else if($mes=='3'){$mes_envio='Marzo'; return $mes_envio;}
		else if($mes=='4'){$mes_envio='Abril'; return $mes_envio;}
		else if($mes=='5'){$mes_envio='Mayo'; return $mes_envio;}
		else if($mes=='6'){$mes_envio='Junio'; return $mes_envio;}
		else if($mes=='7'){$mes_envio='Julio'; return $mes_envio;}
		else if($mes=='8'){$mes_envio='Agosto'; return $mes_envio;}
		else if($mes=='9'){$mes_envio='Septiembre'; return $mes_envio;}
		else if($mes=='10'){$mes_envio='octubre'; return $mes_envio;}
		else if($mes=='11'){$mes_envio='Noviembre'; return $mes_envio;}
		else if($mes=='12'){$mes_envio='Diciembre'; return $mes_envio;}
}
//*******************************************************************************************************
?>
<script>
function comprobarEstado(){
	$.ajax({
			url : 'proceso/comprobarEstado.php',
			type: 'POST',
			}).done(function(resp){
				console.log("comprobarEstado= "+resp);
				resp = parseInt(resp);
				if(resp > 0){
					$("#spanDetener").show();
				}else{
					$("#spanDetener").hide();
				}
				if(resp==3){
					$("#listo").show();
					$("#spanPasar").show();
				}else{
					$("#spanPasar").hide();
					$("#listo").hide();
				}
			});
		setTimeout("comprobarEstado()",5000);
}
</script>
<script>
$(document).ready(function(){  
	comprobarEstado();
	
	 $("#pasar").on("click",function() {
		$.ajax({
					data:{"estado":4},
					url : 'proceso/var_procesar.php',
					type: 'POST',
				}).done(function(resp){
					if(resp =='0'){
						alerta("Problemas al traspasar la base de datos");
					}else if(resp =='1'){
						alerta("Procesando................");
					}else{
						alerta("Problemas al traspasar la base de datos");
					}
				});		 
	 });
	 
	 $("#detener").on("click",function() {
		$.ajax({
					data:{"estado":0},
					url : 'proceso/var_procesar.php',
					type: 'POST',
				}).done(function(resp){
							if(resp =='0'){
						alerta("Problemas al detener el proceso");
					}else if(resp =='1'){
						alerta("Deteniendo");
					}else{
						alerta("Problemas al detener el proceso");
					}
				});		 
	 });
	 
    $("#envio").on("click",function() {  
		$.ajax({
			data:{"estado":1},
			url : 'proceso/var_procesar.php',
			type: 'POST',
		}).done(function(resp){
				console.log(resp);
			if(resp =='0'){
				alerta("Problemas al Procesar favor comunicarse con el Encargado");
			}else if(resp =='1'){
				alerta("Procesando................");
			}else{
				alerta("Problemas al Procesar favor comunicarse con el Encargado");
			}
		});
    });  
 


}); 

</script>
</head>

<body>

<?php
include("banner.php");

$query = mysql_query("SELECT * FROM presupuesto_fecha");
$row = mysql_fetch_array($query);
$mes_numero = $row[1];
$ano = $row[2];

$queryEstado = mysql_query("");
$rowEstado = mysql_fetch_array($queryEstado);
?>
<input type="hidden" name="mesactual" value="<?php echo $mes_numero;?>">

<div id="principal">

	<form name="" class="proceso" id="proceso" method="post" action="var_proceso.php">
		<div style="text-align: center; margin: 25px 0px 40px 0px" ><label class="titulo">Procesar Mes: <?php $mes_cambiado = cambio($mes_numero + 1);
				echo $mes_cambiado;?></label></div>
	<p id='listo' style='display:none; text-align:center; padding-bottom: 25px;'>Esta listo el proceso. hacer click <a href='ppto.php?modulo=9'>aqu&iacute;</a> para revisar</p>
	<div style='text-align: center'><button type="button" name="envio" class="myButton" id="envio">Procesar </button><span id='spanDetener' style='display:none;'><button type="button" name="detener" class="myButton" id="detener" >Detener </button></span><span style='display:none;' id='spanPasar'><button type="button" name="pasar" class="myButton" id="pasar">trasladar base de datos </button></span></div>
		
			

		
	</form>
</div>



</body>
</html><?php
}
else{
echo "<script>redireccion();</script>";
}