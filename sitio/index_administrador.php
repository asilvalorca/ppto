<?php
session_start();
$nombre_usuario = $_SESSION['ppto_nombre']; // nombre del usuario
$usuario = $_SESSION['ppto_usuario'];
$id_usuario = $_SESSION['ppto_id_usuario'];
$perfil = $_SESSION['ppto_perfil']; //sacar perfil de usuario
$id_perfil = $_SESSION['ppto_id_perfil']; //sacar perfil de usuario

$id_usuario = $_SESSION['ppto_id_usuario'];
$usuario = $_SESSION['ppto_usuario'];

include("conectar.php");

$meses = array("","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE", "DICIEMBRE");
$dia = date('d');
$mes = date("n");
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Reporte Mensual de Contrato</title>
		<meta name="description" content="">
		<meta name="author" content="Andres Silva L">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
		<script language="javascript" src='js/jquery-ui-1.9.2.custom.min.js'></script><!--//Incluye el framework de jquery-ui-->
		<link rel='stylesheet' href='css/start/jquery-ui-1.10.4.custom.min.css'><!-- Incluye el css del jquery UI-->
		<script src="js/jquery.Rut.js" type="text/javascript"></script> 
		 <link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
	    <script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxexpander.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxdraw.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxgauge.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxcheckbox.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxradiobutton.js"></script>
	    <script type="text/javascript" src="jqwidgets/jqxslider.js"></script>
	    <script type="text/javascript" src="js/validaNumeroDecimal.js"></script>
	    <link rel='stylesheet' href='css/error.css'><!-- Incluye el css del error-->
	   	<link rel='stylesheet' href='css/styloPagina.css'><!-- Incluye el css del jquery UI -->
		<link rel="stylesheet" type="text/css" href="menu/ddlevelsfiles/ddlevelsmenu-base.css" />
		<link rel="stylesheet" type="text/css" href="menu/ddlevelsfiles/ddlevelsmenu-topbar.css" />
		<link rel="stylesheet" type="text/css" href="menu/ddlevelsfiles/ddlevelsmenu-sidebar.css" />
		<script type="text/javascript" src="menu/ddlevelsfiles/ddlevelsmenu.js"></script>
	<style>
		*{
			margin: 0px;
			padding: 0px;
		}
		.bordeInferior{
			border-bottom: 1px solid #000;
		}
		
		.inputSinBordes{
			border: 0px;	
		}
		.uno, .dos, .tres, .cuatro,.cinco, .seis, .siete{
			display:none;
			margin-left: 50px;
			
			margin-bottom: 25px;
		}
		#principal{
			display:block;
			margin-left: auto;
			margin-right: auto;
			width: 90%;
			border:1px solid;
			-webkit-border-top-left-radius: 10px; 
			-webkit-border-top-right-radius: 10px; 
			-moz-border-radius-topleft: 10px; 
			-moz-border-radius-topright: 10px;
			 border-top-left-radius: 10px;
			border-top-right-radius: 10px;
			background: #f1f1f1;
		}
		#principal > div{
			margin-left: 25px;
		}
		.flechas{
			width: 14px;
			height: 14px;
		}
		.secciones{
			cursor: hand;
			cursor: pointer;
			font-size: 20px;
			padding-bottom:20px;
		}
		.tabla{
			border:0px solid;
			width: 80%;
			margin-left: 15px;
			background:#ffffff;
			-webkit-border-radius: 20px;
			-moz-border-radius: 20px;
			border-radius: 20px;
			padding-top: 10px;
			padding-bottom: 10px;
			border: 1px solid #1e2654;
		}
		.tabla2{
			border:0px solid;
			width: 80%;
			margin-left: 15px;
			background:#ffffff;
		}
		.tabla td div{
			margin-left: 15px;
		}
		.cursor{
			cursor: hand;
			cursor: pointer;
		}
	</style>
	<script>
		function informacionFaena(id){
			
			console.log("id"+id);
			
			$.getJSON( "datos_faena_sup_ajax.php?id="+id, function( data1 ) {
			 			 
				  $.each( data1, function( key, val ) {	
				  	console.log(val['nombre_contrato'].toUpperCase());	 		 
				  	$("#nombre_contrato").val(val['nombre_contrato'].toUpperCase());
				  	$("#cliente").val(val['nombre_cliente'].toUpperCase());
				  
				  	$("#dotacion_contrato").val(val['dotacion_contractual'].toUpperCase());
				   	$("#anegocio_principal").val(val['nombre_faena'].toUpperCase());
				  	 	
				  });
			 });	  
			$.getJSON( "datos_equipos_ajax.php?id="+id, function( data2 ) {
			 		cont = 1;	 
				  $.each( data2, function( key, val ) {	
				  	opcion = "<tr id='estadoequipos"+cont+"'>"+
								"<td><input type='text' name='marca"+cont+"' id='marca"+cont+"' data-contador='"+cont+"' value='"+val['marca_equipo']+"' class='inputSinBordes '></td>"+
								"<td><input type='text' name='modelo"+cont+"' id='modelo"+cont+"' data-contador='"+cont+"' value='"+val['modelo_equipo']+"' class='inputSinBordes'></td>"+
								"<td><input type='text' name='patente"+cont+"' id='patente"+cont+"' data-contador='"+cont+"' value='"+val['patente_equipo']+"' class='inputSinBordes'></td>"+
								"<td><input type='text' name='tipo"+cont+"' id='tipo"+cont+"' data-contador='"+cont+"' value='"+val['tipo_equipo']+"' class='inputSinBordes'></td>"+
								"<td><input type='text' name='estado"+cont+"' id='estado"+cont+"' data-contador='"+cont+"' class='inputSinBordes'></td>"+
								"<td><div style='text-align:center'><img src='imagenes/eliminar.png' class='cursor estadoequiposEliminar' style='width:15px; height:15px;' name='eliminar"+cont+"' id='patente"+cont+"' data-contador='"+cont+"' ></div></td>"+
							 "</tr>";
				  	 	cont++;
				  });
				  $("#estadoequipos").append(opcion);
			  });
		}
	</script>
	<script>
		$(document).ready(function(){
			$("#estadoequipos").on("click",".estadoequiposEliminar",function(){
				console.log("aaaa");
			});
			var data;
			$.getJSON( "anegocio_ajax.php", function( data ) {
			 
				 var cont = 0;
				 var opcion ='';
				  $.each( data, function( key, val ) {			   
				 	 opcion = opcion + "<option value='"+val['id']+"'>"+val['anegocio']+"</option";
				  	 cont++;			    	
				  });
				  
				 // console.log(data[0]['anegocio']+"deberia");
				  if(cont>1){
				  	$("#div_anegocio").show();
				 	 $("#anegocio").append(opcion);			  	
				  }else{
				  	 informacionFaena(data[0]['id']);
				  }		  
			 });
			
			$("#anegocio").on("change",function(){
				 informacionFaena($(this).val());
			});
			
			$("input[type='text']").keyup(function() {
				$(".error").remove();
			});
			
			$("input[type='checkbox']").on("click",function() {
				$(".error").remove();
			});
			
			$("#enviar").on("click",function(){
				
				if($("#dotacion_real").val()==''){
					
						$("#dotacion_real").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
				}else{
					dotacion_real = $("#dotacion_real").val();
				}
				
				if($("#ind_equi_cli").val()==''){
					if($("#ind_equi_cli_check").is(":checked")){
						console.log("nada que hacer aqui");
						ind_equi_cli = -1;
					}else{
						$("#ind_equi_cli").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
					}
				}else{
					ind_equi_cli = $("#ind_equi_cli").val();
				}
				
				if($("#cons_neum_real").val()==''){
					if($(".inpDos_check").is(":checked")){
						console.log("nada que hacer aqui");
						cons_neum_real = -1;
					}else{
						$("#cons_neum_real").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
					}
				}else{
					cons_neum_real = $("#cons_neum_real").val();
				}
				
				if($("#cons_neum_presu").val()==''){
					if($(".inpDos_check").is(":checked")){
						console.log("nada que hacer aqui");
						cons_neum_presu = -1;
					}else{
						$("#cons_neum_presu").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
					}
				}else{
					cons_neum_presu = $("#cons_neum_presu").val();
				}
				
				if($("#rend_neum_real").val()==''){
					if($(".inpTres_check").is(":checked")){
						console.log("nada que hacer aqui");
						rend_neum_real = -1;
					}else{
						$("#rend_neum_real").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
					}
				}else{
					rend_neum_real = $("#rend_neum_real").val();
				}
				
				if($("#rend_neum_presup").val()==''){
					if($(".inpTres_check").is(":checked")){
						console.log("nada que hacer aqui");
						rend_neum_presup = -1;
					}else{
						$("#rend_neum_presup").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
					}
				}else{
					rend_neum_presup = $("#rend_neum_presup").val();
				}
				
				if($("#med_aro").val()==''){
					if($(".inpTres_check").is(":checked")){
						console.log("nada que hacer aqui");
						med_aro = -1;
					}else{
						$("#med_aro").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
					}
				}else{
					med_aro = $("#med_aro").val();
				}
				
				if($("#cant_acc_tp").val()==''){
					
						$("#cant_acc_tp").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
				}else{
					cant_acc_tp = $("#cant_acc_tp").val();
				}
				
				if($("#dias_perdidos").val()==''){
					
						$("#dias_perdidos").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
				}else{
					dias_perdidos = $("#dias_perdidos").val();
				}
				
				if($("#cuid_mambiente").val()==''){
					
						$("#cuid_mambiente").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
				}else{
					cuid_mambiente = $("#cuid_mambiente").val();
				}
				
				if($("#aseg_calid").val()==''){
					
						$("#aseg_calid").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
				}else{
					aseg_calid = $("#aseg_calid").val();
				}
				
				
				if($("#prog_prev").val()==''){
					
						$("#prog_prev").focus().after("<span class='error'>Debe Ingresar el valor</span>");
						return false;
				}else{
					prog_prev = $("#prog_prev").val();
				}
				
				
				parametros = {"dotacion_real":dotacion_real,"ind_equi_cli":ind_equi_cli, "cons_neum_real":cons_neum_real, "cons_neum_presu":cons_neum_presu, "rend_neum_real":rend_neum_real, "rend_neum_presup":rend_neum_presup, "med_aro":med_aro,"cant_acc_tp":cant_acc_tp, "dias_perdidos":dias_perdidos, "cuid_mambiente": cuid_mambiente, "aseg_calid":aseg_calid, "prog_prev":prog_prev};
				
				$.ajax({
					url:'var_ingreso_admin.php',
					type:'POST',
					data: parametros
					}).done(function(resp){
						console.log("aaaaa"+resp);
					});
			});
			
			
			$(".secciones").on("click",function(){
				var seccion = $(this).data("seccion");
				var funcion = $(this).data("funcion");
				
				if(funcion=='0')
				{
					$("."+seccion).show();
					$(this).data("funcion",'1');
					$(this).children(".flechas").attr("src","imagenes/subir.png");
				}else if(funcion=='1'){
										
					$("."+seccion).fadeOut();
					$(this).data("funcion",'0');
					$(this).children(".flechas").attr("src","imagenes/bajar.png");
				}
			});
			$(".agregar").on("click",function(){
				var id = $(this).data("id");
				
				$("#"+id).append('<tr><td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr>');
			});
			
			$("input[type='checkbox").on("click",function(){
				var id = $(this).data("id");
				if($(this).is(':checked') )
				{
					console.log("aaaa"+id);
					$("."+id).prop('disabled', true);
				}else{
					console.log("bbbbb"+id);
					$("."+id).prop('disabled', false);
				}
			});
		});
	</script>	
	</head>

	<body>
		<?php
include("banner.php");
?>
		<div>
			
			
			<div id='principal'>
				
				<h2 style='text-align:center;display: block; margin-left: auto; margin-right: auto;padding-bottom: 17px; background:#1e2654; color: #fff; -webkit-border-top-left-radius: 10px; -webkit-border-top-right-radius: 10px; -moz-border-radius-topleft: 10px; -moz-border-radius-topright: 10px; border-top-left-radius: 10px;
border-top-right-radius: 10px;'>Reporte Mensual de Contrato</h2>
				<button id='enviar'>Enviar</button>
				<div id='div_anegocio' style='display: none;'><label>Seleccione Area de Negocio</label><select name='anegocio' id='anegocio'></select></div>
				<div data-funcion='0' data-seccion='uno' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Informaci&oacute;n Faenas</span>    </div>
				<div class='uno'>
				<table class='tabla'>
					<tr>
						<td><div>Nombre Contrato</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes' name='nombre_contrato' id='nombre_contrato' readonly /></div></td>
					</tr>
					<tr>
						<td><div>Faena</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes' name='anegocio_principal' id='anegocio_principal' readonly /></div></td>
					</tr>
					<tr>
						<td><div>Cliente</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes' name='cliente' id='cliente' readonly  /></div></td>
					</tr>
					<tr>
						<td><div>Responsable</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes' name='responsable' id='responsable' value='<?php echo $nombre_usuario ?>' readonly /></div></td>
					</tr>
					<tr>
						<td><div>Dotaci&oacute;n Real del Mes</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes validarNumero' data-decimal='0' name='dotacion_real' id='dotacion_real'  /></div></td>
					</tr>
					<tr>
						<td><div>Dot. Contractual/mes</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes' name='dotacion_contrato' id='dotacion_contrato' readonly /></div></td>
					</tr>
					<tr>
						<td><div>Fecha</div></td><td class='bordeInferior'><div><input type='text' class='inputSinBordes' name='' id='' value='<?php echo $meses[$mes]."-15"; ?>' readonly /></div></td>
					</tr>
				</table>
			</div>
			<div data-funcion='0' data-seccion='dos' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Kpi Faenas</span></div>   
			<div class='dos'>
				<table class='tabla'>
					<tr>
						<td>1.- Indisponibilidad de equipos del cliente</td><td ><input type='text' class='validarNumero inpUno' data-decimal='1' name='ind_equi_cli' id='ind_equi_cli' />%</td>
					</tr>
					<tr>
						<td><div style='padding-bottom: 25px;'>(N/A si no aplica)<input type='checkbox' class='validarNumero' data-decimal='0' data-id='inpUno' data-cant='1' id='ind_equi_cli_check' ></div></td><td class=''></td>
					</tr>
					<tr>
						<td>2.- Consumo de neum&aacute;ticos en el mes</td><td class=''><input type='text' name='cons_neum_real' data-decimal='0' class='validarNumero inpDos'  id='cons_neum_real'  />(uni.)Real</td>
					</tr>
					<tr>
						<td><div style='padding-bottom: 25px;'>(N/A si no aplica)<input type='checkbox' data-id='inpDos' data-cant='2' class='inpDos_check'></div></td><td class=''><input type='text'  name='cons_neum_presu' id='cons_neum_presu' data-decimal='0' class='inpDos validarNumero'  />(uni.)Presupuesto</td>
					</tr>
					<tr>
						<td>3.- Consumo de neum&aacute;ticos en el mes</td><td class=''><input type='text' class='inpTres validarNumero' data-decimal='0' name='rend_neum_real' id='rend_neum_real' />(uni.)Real</td>
					</tr>
					<tr>
						<td><div style='padding-bottom: 25px;'>(N/A si no aplica)<input type='checkbox' data-id='inpTres' class='inpTres_check'></div></td><td class=''><input type='text' class='inpTres validarNumero' data-decimal='0' name='rend_neum_presup' id='rend_neum_presup' />(uni.)Presupuesto</td>
					</tr>
					<tr>
						<td><div style=''></div></td><td class=''><input type='text' class='inpTres validarNumero' data-decimal='0' name='med_aro' id='med_aro' />(in)Medida del Aro</td>
					</tr>
					<tr>
						<td style='padding-top: 25px;'>4.- Numero de accidentes con tiempo perdido</td><td class='' style='padding-top: 25px;'><input type='text'  class='validarNumero' data-decimal='0' name='cant_acc_tp' id='cant_acc_tp' />Accidentes CTP</td>
					</tr>
					<tr>
						<td>Observaciones</td><td  class='bordeInferior'><input type="text" style='width: 100%'></td>
					</tr>
					<tr>
						<td><div style='padding-bottom: 25px; padding-top: 25px;'>5.- Dias Perdidos</div></td><td ><input type='text' name='dias_perdidos' id='dias_perdidos' />D&iacute;as</td>
					</tr>
					<tr>
						<td>6.- Cuidado del medio ambiente</td><td ><input type='text' name='cuid_mambiente' id='cuid_mambiente' />%</td>
					</tr>
					<tr>
						<td colspan="2">Cumplimiento de normas,estandares y mejores practicasen materia de cuidado del medio ambiente</td>
					</tr>
					<tr>
						<td ><div style=' padding-top: 25px;'>7.- Aseguramiento de la calidad</div></td><td style=' padding-top: 25px;'><input type='text' name='aseg_calid' id='aseg_calid' />%</td>
					</tr>
					<tr>
						<td colspan="2">Grado de cumplimiento auditoria de cliente (Interno si no hay)</td>
					</tr>
						<td ><div style=' padding-top: 25px;'>8.- Cumplimiento del programa de prevenci&oacute;n de riesgo</div></td><td style=' padding-top: 25px;'><input type='text' name='prog_prev' id='prog_prev' />%</td>
					</tr>
					
				</table>
			</div>
			<div data-funcion='0' data-seccion='tres' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Inconformidades registradas</span>   </div>
			<div class='tres'>
				<div class='cursor agregar' data-id='inconformidades'><img src='imagenes/agregar.png' class="flechas">Agregar m&aacute;s campos</div>
				<table class='tabla2' border='1' id='inconformidades' style="background: #fff;" cellpadding="0" cellspacing="0">
					<tr>
						<td><div style='text-align: center'>N&deg;</div> </td><td ><div style='text-align: center'>Fecha</div></td><td ><div style='text-align: center'>Detalle</div></td>
					</tr>
					<tr>
						<td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
					</tr>
					
				</table>
			</div>
			<div data-funcion='0' data-seccion='cuatro' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Estado de los equipos</span>   </div>
			<div class='cuatro'>
				<div class='cursor agregar' data-id='estadoequipos'><img src='imagenes/agregar.png' class="flechas">Agregar m&aacute;s campos</div>
				<table class='tabla2' border='1' id='estadoequipos' style="background: #fff;" cellpadding="0" cellspacing="0">
				<tr>
					<td><div style='text-align: center'>Marca</div> </td><td ><div style='text-align: center'>Modelo</div></td><td ><div style='text-align: center'>Patenteo N&deg; serie</div></td><td ><div style='text-align: center'>Tipo</div></td><td><div style='text-align: center'>Estado</div></td><td><div style='text-align: center'>Eliminar</div></td>
				</tr>
				
					
				</table>
			</div>
			<div data-funcion='0' data-seccion='cinco' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Equipos Criticos</span>   </div>
			<div class='cinco'>
				<table class='tabla'>
					<tr>
						<td>11.- Disponibilidad de Equipos criticos</td><td ><input type='text' name='' id='' /></td>
					</tr>
					<tr>
						<td>12.- Cumplimiento del programa de mantenimiento (Equipos Criticos)</td><td ><input type='text' name='' id='' /></td>
					</tr>
				</table>
			</div>
			<div data-funcion='0' data-seccion='seis' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Personal por sobre dotaci&oacute;n</span>   </div>
			<div class='seis'>
				<div class='cursor agregar'  data-id='personaldotacion' ><img src='imagenes/agregar.png' class="flechas">Agregar mas campos</div>
				<table class='tabla2' id='personaldotacion' border='1' style="background: #fff;" cellpadding="0" cellspacing="0">
				<tr>
						<td><div style='text-align: center'>Nombre</div> </td><td ><div style='text-align: center'>Rut(12.345.678-9)</div></td><td ><div style='text-align: center'>Cargo</div></td><td ><div style='text-align: center'>Motivos de contrataci&oacute;n</div></td>
					</tr>
					<tr>
						<td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
					</tr>
					
				</table>
			</div>
			<div data-funcion='0' data-seccion='siete' class='secciones'><img src='imagenes/bajar.png'  class='flechas'><span>Personal despedido</span>   </div>
			<div class='siete'>
				<div class='cursor agregar' data-id='personaldespedido'><img src='imagenes/agregar.png' class="flechas">Agregar mas campos</div>
				<table class='tabla2' border='1' id='personaldespedido' style="background: #fff;" cellpadding="0" cellspacing="0">
				<tr>
						<td><div style='text-align: center'>Nombre</div> </td><td ><div style='text-align: center'>Rut(12.345.678-9)</div></td><td ><div style='text-align: center'>Cargo</div></td><td ><div style='text-align: center'>Motivos del despido</div></td>
					</tr>
					<tr>
						<td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
					</tr>
					
				</table>
			</div>
									
			</div>

			
		</div>
	</body>
</html>
