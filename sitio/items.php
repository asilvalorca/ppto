<?php 
include("conectar.php");
include("bd.php");

$anegocio = $_GET['a'];
$oc = $_GET['o'];
$flex = $_GET['f'];

function formateoNumero($num)
{
	
	return number_format($num, 0, '', '.');
}

function cambioFecha($fecha){
	
 $fechax = explode( '-',$fecha);
        $fecha_temp = $fechax[2]."/".$fechax[1]."/".$fechax[0];
		return $fecha_temp;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Detalle OC</title>
<script src='js/jquery-1.8.3.min.js'></script><!--Incluye el framework de jquery -->
<script src='js/jquery-ui-1.9.2.custom.min.js'></script><!--//Incluye el framework de jquery-ui-->
<link rel='stylesheet' href='css/ui-lightness/jquery-ui-1.9.2.custom.min.css'><!-- Incluye el css del jquery UI-->
<script language="javascript" src='js/jtooltip.js'></script>
<link rel='stylesheet' href='css/jtooltip.css'><!-- Incluye el css del jquery UI-->


<script type="text/javascript">
function fnc() {
document.getElementById('table-scroll').onscroll = function() {

document.getElementById('fixedY').style.top = document.getElementById('table-scroll').scrollTop + 'px';
document.getElementById('fixedX').style.left = document.getElementById('table-scroll').scrollLeft + 'px';

};
}

window.onload = fnc;
</script>
<script>
function abrir(sol, oc){

//document.body.scroll='no';

link='solicitudes.php?oc='+oc+'&sol='+sol;
	window.open(link,'solicitud','width=1000,height=550, scrollbars=yes');


}



</script>
<style>
body{
	font-size: 12px;
	background-color:#ededed;
}
.cursor{
cursor: pointer;
cursor: hand;	

}
#table-scroll {
	width: 100%;
	height: auto;
	overflow: auto;
}


#fixedY{
	position: relative;
	top: 0;
	z-index: 99;
	background-color: red;
}

#fixedY table{
	border-collapse: collapse;
	width: 100%;
}

#cuerpoDatos {
	width: 100%;
}

#cuerpoDatos > div{
	float: left;
}

#cuerpoDatos > div#nofixedX{
	width: 100%;
}

#cuerpoDatos > div#nofixedX table{
	border-collapse: collapse;
width: 100%;
}
.thtitulo
{
	text-align:center; 
	background: rgba(212,228,239,1);
	background: -moz-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(212,228,239,1)), color-stop(100%, rgba(134,174,204,1)));
	background: -webkit-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: -o-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: -ms-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: linear-gradient(to bottom, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d4e4ef', endColorstr='#86aecc', GradientType=0 );
	border: 0px solid;
	width: 100%;
	font-weight: bold;
	padding-top: 2px;
	padding-bottom: 3px;
}

#tab1
{
	background: rgba(212,228,239,1);
	background: -moz-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(212,228,239,1)), color-stop(100%, rgba(134,174,204,1)));
	background: -webkit-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: -o-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: -ms-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	background: linear-gradient(to bottom, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d4e4ef', endColorstr='#86aecc', GradientType=0 );
}

.t1{width:10%;}
.t2{width:15%;}
.t3{width:10%;}
.t4{width:10%;}
.t5{width:23%;}
.t6{width:22%;}
</style>
</head>

<body>
<div id='principal' >


<div><p style='font-weight: bold; font-size: 16px'>Orden de Compra: <?php echo $flex ?></p></div>
<div id="table-scroll" >

<div id="fixedY">
	<table border='1' id='tab1'>
		<thead>
		<tr>
			<th class='t1'>Solicitud</th>
			<th class='t2'>&Aacute;rea de Negocio</th>
			<th class='t3'>Solicitante</th>
			<th class='t4'>Fecha Emisi&oacute;n.</th>
		</tr>
<div id="cuerpoDatos">

<div id="nofixedX">
<table border='1' style='background: #fff;'>
<tbody>
<tr>		
<?php

		$queryOC = $bd->query("SELECT *
								FROM `compras_ocompra` a
								INNER JOIN compras_ocompra_detalle b ON a.id = b.id_oc
								INNER JOIN compras_item c ON c.id_item = b.id_item
								INNER JOIN compras_sol_compra d ON c.id_sol = d.id_sol
								INNER JOIN compras_anegocio f ON f.anegocio = d.anegocio
								WHERE a.id = '".$oc."'
								GROUP BY c.id_sol");
		while ($rowOC = $queryOC->fetch_array(MYSQLI_BOTH))
		{
			$queryUsuario = $bd->query("SELECT * FROM `compras_usuarios` WHERE `id_user` ='".$rowOC['id_user']."'");
			$rowUsuario = $queryUsuario->fetch_array(MYSQLI_BOTH);

			if($anegocio==$rowOC['numaneg'])
			{?>
			<tr>
				<td class="t1" onclick='abrir("<?php echo $rowOC['id_sol']; ?>","<?php echo $oc ?>")' class='cursor' title='Haz Click Sobre el N&deg; de la Solicitud para ver m&aacute;s detalles' ><div style='text-align:center' class='cursor'><?php echo $rowOC['id_sol']; ?></div></td>
				<td class="t2" title='<?php echo $rowOC['anegocio']; ?>' ><?php echo $rowOC['anegocio']; ?></td>
				<td class="t3" title='<?php echo $rowUsuario['usuario']; ?>' ><?php echo $rowUsuario['usuario']; ?></td>
				<td class="t4" title='<?php echo cambioFecha($rowOC['fecha_emision']); ?>'><div style='text-align:center'><?php echo cambioFecha($rowOC['fecha_emision']) ?></div></td>
				
			</tr><?php 
			}
			else
			{ ?>
				<tr style='color: rgba(0, 0, 0, 0.2);'>
				<td class="t1"><?php echo $rowOC['id_sol']; ?></td>
				<td class="t2"><?php echo $rowOC['anegocio']; ?></td>
				<td class="t3"><?php echo $rowUsuario['usuario']; ?></td>
				<td class="t4"><div style='text-align:center'><?php echo cambioFecha($rowOC['fecha_emision']) ?></div></td>
				
			</tr><?php 
			}
			
		}?>
			
	</tbody>
</table>
</div>

</div>
</div>
</div>



</div>
</body>
</html>