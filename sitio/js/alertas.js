function alerta(mensaje)
{
	if($(window).width() <= 600){
		var ancho = 300;
	}
	else{
		var ancho = 420;
	}
	
	$('<div title="Alerta">'+mensaje+'</div>').dialog({ modal: true,
												width       :   ancho,
												heigth      :   120,
												resizable: false,
												
												buttons: [{text: "Aceptar",click: function() {
												$( this ).dialog( "close" );}}]}).parent('.ui-dialog').find('.ui-dialog-titlebar-close').hide();

}